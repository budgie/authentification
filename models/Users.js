const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const UsersSchema = new Schema({
	email: String,
	hash: String,
	salt: String
});

UsersSchema.methods.validatePassword = function(password) {
	const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
	return this.hash === hash;
};

UsersSchema.methods.generateJWToken = function() {
	const today = new Date();
	const expriationDate = new Date(today);
	expriationDate.setDate(today.getDate() + 60);

	return jwt.sign(
		{
			email: this.email,
			id: this._id,
			exp: parseInt(expriationDate.getTime() / 100, 10)
		},
		'secret'
	);
};

UsersSchema.methods.toAuthJSON = function() {
	return {
		_id: this.id,
		email: this.email,
		token: this.generateJWToken()
	};
};
